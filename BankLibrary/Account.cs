using System;

namespace BankLibrary
{
  public abstract class Account
  {
    private int _accountNumber;
    private string _holdersName;
    private decimal _balance;

    public Account(int accountNumber, string holdersName, decimal balance)
    {
      _accountNumber = accountNumber;
      _holdersName = holdersName;
      _balance = balance;
      //Console.WriteLine("Coming from Account class Constructor");
    }

    // Readonly property
    public int AccountNumber
    {
      get { return _accountNumber; }
    }

    public string HoldersName
    {
      get { return _holdersName; }
      set { _holdersName = value; }
    }

    public decimal Balance
    {
      get { return _balance; }
      protected set { _balance = value; }
    }

    public object this[int position]
    {
      get
      {
        if (position == 0)
        { return _accountNumber; }
        else if (position == 1)
        { return _holdersName; }
        else if (position == 2)
        { return _balance; }
        throw new Exception($"No data found at the give position: {position}");
        //return $"No data found at the give position: {position}";
      }
    }


    public virtual void Deposit(decimal amount)
    {
      Console.WriteLine("Coming from Account class deposit method");
      if (amount < 0)
      {
        //Console.WriteLine("Negative amount is not allowed");
        throw new ArgumentException("Negative amount is not allowed");
        //return;
      }
      _balance += amount;
    }

    public abstract void Withdraw(decimal amount);

    public override string ToString()
    {
      return $"AccountNumber: {_accountNumber}, HoldersName: {_holdersName}, Balance: {_balance}";
    }
  }
}