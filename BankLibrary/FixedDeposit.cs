using System;

namespace BankLibrary
{
  public sealed class FixedDeposit : Account
  {
    private readonly decimal _openingBalance;
    public FixedDeposit(int accountNumber, string holdersName, decimal balance) : base(accountNumber, holdersName, balance)
    {
      _openingBalance = 3000;
      if (balance < _openingBalance)
      {
        throw new ArgumentException($"Opening balance cannot be less than {_openingBalance}");
        //return;
      }
    }

    public override void Deposit(decimal amount)
    {
      throw new ArgumentException("Deposit not supported in FixedDeposit");
    }
    public override void Withdraw(decimal amount)
    {
      throw new ArgumentException("Withdraw not supported in FixedDeposit");
    }
  }
}