using System;

namespace BankLibrary
{
  public class SavingsAccount : Account
  {

    private readonly decimal _minimumBalance;
    public SavingsAccount(int accountNumber, string holdersName, decimal balance) : base(accountNumber, holdersName, balance)
    {
      _minimumBalance = 500;
      //Console.WriteLine("Coming from SavingsAccount class Constructor");
    }

    public override void Withdraw(decimal amount)
    {
      //Console.WriteLine("Coming from SavingsAccount class withdraw method");
      if (amount < 0)
      {
        //Console.WriteLine("Negative amount is not allowed");
        throw new ArgumentException("Negative amount is not allowed");
        //return;
      }
      if ((Balance - amount) < _minimumBalance)
      {
        throw new ArgumentOutOfRangeException($"Insufficient funds in SavingsAccount, Can withdraw upto: {(Balance - _minimumBalance)}");
        //return;
      }

      Balance = (Balance - amount);
    }
  }
}