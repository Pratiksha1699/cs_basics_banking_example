using static System.Console;
namespace BankLibrary
{
  public static class printer
  {
    public static void Print(string pattern, string header, string message, int nop)
    {
      WriteLine();
      for (int i = 0; i <= nop / 2; i++)
      {
        Write($"{pattern}");
      }
      Write($"{header}");
      for (int i = 0; i <= nop / 2; i++)
      {
        Write($"{pattern}");
      }
      WriteLine();
      WriteLine($"{message}");

    }
    public static void Printheader(string pattern, string header, int nop)
    {
      WriteLine();
      for (int i = 0; i <= nop / 2; i++)
      {
        Write($"{pattern}");
      }
      Write($"{header}");
      for (int i = 0; i <= nop / 2; i++)
      {
        Write($"{pattern}");
      }
      WriteLine();
    }
    public static void Printfooter(string pattern, string header, int nop)
    {
      WriteLine();
      for (int i = 0; i <= nop; i++)
      {
        Write($"{pattern}");
      }
      for (int i = 0; i <= header.Length; i++)
      {
        Write($"{pattern}");
      }
      WriteLine();
    }
  }
}