using System;

namespace BankLibrary
{
  public class CurrentAccount : Account
  {
    private readonly decimal _odlimit;
    public CurrentAccount(int accountNumber, string holdersName, decimal balance, decimal odLimit) : base(accountNumber, holdersName, balance)
    {
      _odlimit = odLimit;
    }

    public CurrentAccount(int accountNumber, string holdersName, decimal balance) : this(accountNumber, holdersName, balance, 0) { }

    public override void Withdraw(decimal amount)
    {
      //Console.WriteLine("Coming from CurrentAccount class withdraw method");

      if (amount < 0)
      {
        throw new ArgumentException("Negative amount is not allowed");
        //return;
      }
      if (((Balance + _odlimit) - amount) < 0)
      {
        throw new ArgumentOutOfRangeException($"Insufficient funds in CurrentAccount, Current Balance is: {Balance}, can withdraw upto: {(Balance + _odlimit)}");
        //return;
      }
      Balance = Balance - amount;
    }
  }
}