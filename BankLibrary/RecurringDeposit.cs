using System;

namespace BankLibrary
{
  // public class RecurringDeposit : FixedDeposit // Cannot inherit from FixedDeposit as it is a sealed class
  public class RecurringDeposit : Account
  {
    public RecurringDeposit(int accountNumber, string holdersName, decimal balance) : base(accountNumber, holdersName, balance)
    { }

    public override void Withdraw(decimal amount)
    {
      throw new ArgumentException("Withdraw not supported in FixedDeposit");
    }
  }
}