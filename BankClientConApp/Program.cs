﻿using System;

using BankLibrary;

namespace BankClientConApp
{
  class Program
  {
    static void MainTryCatchFinally(String[] args)
    {
      int num = 10, div = 5;
      try
      {

        Console.WriteLine($"num: {num} and div: {div} and result: {num / div} ");
        return;
        //Console.WriteLine("Bye Bye...!!!");

      }
      catch (Exception ex)
      {
        //printer.Print("*", ex.Message, "Using Message", 100);
        //printer.Print("*", ex.StackTrace, "Using StackTrace", 100);
        printer.Print("*", ex.ToString(), "Using ToString()", 100);
        Console.WriteLine("Bye bye from catch");
        Console.WriteLine($"Value of div from Catch: {div}");
        div = 2;
      }
      finally
      {
        Console.WriteLine("Bye bye from finally");
        Console.WriteLine($"Value of div: {div}");
        Console.ReadKey();
      }
      Console.WriteLine("Bye bye from outstde finally");
      Console.WriteLine($"Value of div: {div}");
    }
    static void Main(string[] args)
    {
      int depositAmount = 500;
      int withdrawAmount = 500;
      Account acc = null; // Reference of Account
      try
      {


        // acc = new SavingsAccount(101, "Tintin", 3000);
        // acc = new CurrentAccount(201, "Jughead", 5000, 3000);
        // acc = new CurrentAccount(201, "Jughead", 3500);
        acc = new FixedDeposit(301, "Phantom", 500);
        // acc = new RecurringDeposit(401, "Heman", 5000);

        Console.WriteLine(acc);
        printer.Printfooter("*", "acc", 100);

        acc.Deposit(depositAmount);
        Console.WriteLine("Balance after a deposit of {0} is: {1}", depositAmount, acc.Balance);
        printer.Printfooter("*", "acc", 100);

        Console.WriteLine("Press any key to continue");
        // Console.ReadKey(true);
        acc.Withdraw(withdrawAmount);
        Console.WriteLine("Balance after a withdraw of {0} is: {1}", withdrawAmount, acc.Balance);
        Console.WriteLine($"acc[0]:{acc[0]}");
        Console.WriteLine($"acc[1]:{acc[1]}");
        Console.WriteLine($"acc[2]:{acc[2]}");
        Console.WriteLine($"acc[12]:{acc[12]}");
      }
      catch (ArgumentOutOfRangeException ex)
      {
        printer.Print("*", ex.ToString(), "Argument Out Of Range Exceptions", 100);
      }
      catch (ArgumentException ex)
      {
        printer.Print("*", ex.ToString(), "Argument Exceptions", 100);
      }
      catch (Exception ex)
      {
        printer.Print("*", ex.ToString(), "Exceptions", 100);
      }
      finally
      {
        Console.WriteLine("cleaning all bank objects");
        depositAmount = 0;
        withdrawAmount = 0;
        acc = null;
      }
    }
  }
}
