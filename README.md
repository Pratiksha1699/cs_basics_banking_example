# Bank Project

#### Project Structure

- BankSolution
  - BankLibrary
  - BankClientConApp

#### Commands for Creating the Projects

- Create a Solution with a folder

  - dotnet new sln --name BankSolution --output BankSolution

- cd BankSolution

- Create a ClassLibrary Project within the BankSolution Folder

  - dotnet new classlib --name BankLibrary --output BankLibrary

- Add BankLibrary to BankSolution

  - dotnet sln add .\BankLibrary\

- Create a ConsoleApp project within the BankSolution Folder

  - dotnet new console --name BankClientConApp --output BankClientConApp

- Add BankClientConApp to BankSolution

  - dotnet sln add .\BankClientConApp\

- Add BankLibrary Reference to BankClientConApp
  - dotnet add .\BankClientConApp\ reference .\BankLibrary

---

#### Convention for naming in CSharp

| Name                    | Private              | Protected   | Public     |
| ----------------------- | -------------------- | ----------- | ---------- |
| ClassName (ProperCase)  | ProperCase           | ProperCase  | ProperCase |
| MethodName (ProperCase) | ProperCase/camelCase | ProperCase  | ProperCase |
| MethodArgs (camelCase)  | camelCase            | camelCase   | camelCase  |
| Properties (ProperCase) | ProperCase           | ProperCase  | ProperCase |
| ClassLevel Variable     | \_camelCase          | \_camelCase | ProperCase |
| MethodLevel Variable    | camelCase            | camelCase   | camelCase  |

#### Git Commands

- git init
- Add gitignore file with Both VisualStudio and VisualStudio code
- git add .
- git commit -a -m "message"
- git checkout -b <branchname>
- git checkout <branchname>
- git branch --list

#### Git Branches

- 01Start
- 02Procedural
- 03Properties
- 04Inheritance
- 05Encapsulation
- 06Polymorphism
- 07Indexers
- 08Static
